import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class MainApp extends StatefulWidget {
  final Connect connect;
  MainApp({Connect Connect}):connect = Connect;

  @override
  MainAppState createState() => new MainAppState();
}

class MainAppState extends State<MainApp> {

  String _filePath, _users;

  @override
  void initState() {
    super.initState();
    _fileList();
  }

  @override
  Widget build(BuildContext context) {
    _users = widget.connect.account.usernames[0];
    if (widget.connect.account.usernames.length > 1)
      for (int i = 1; i < widget.connect.account.usernames.length; i++)
        _users += widget.connect.account.usernames[i];

    double width = MediaQuery
        .of(context)
        .size
        .width;
    double height = MediaQuery
        .of(context)
        .size
        .height;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(_users),
        leading: new IconButton(
          onPressed: () {
            Navigator.of(context).pushNamedAndRemoveUntil(
                '/',
                ((Route<dynamic> route) => false)
            );
          },
          icon: Icon(Icons.undo),
        ),
        backgroundColor: Colors.indigo[600],
      ),
      body: new Center(
//        child: _body(),
//        child: new Text(
//            '0 records',
//            style: new TextStyle(color: Colors.black45)
//        ),
      ),
      floatingActionButton: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new FloatingActionButton(
            child: Icon(Icons.videocam),
            backgroundColor: Colors.indigo[600],
            onPressed: () {
              Navigator.of(context).pushNamed(
                  '/NewVideo',
                  arguments: widget.connect
              );
            },
          ),
          new SizedBox(
            width: width * 0.03,
          ),
          new FloatingActionButton(
            child: Icon(Icons.keyboard_voice),
            backgroundColor: Colors.indigo[600],
            onPressed: () {
              Navigator.of(context).pushNamed(
                  '/NewVideo',
                  arguments: widget.connect
              );
            },
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Future<void> _fileList() async{
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/$_users';
    print(dirPath);
    await Directory(dirPath).delete(recursive: true);

  }
}

class Connect {
  Account account;
  String token;

  Connect({
    this.account,
    this.token
  });

  factory Connect.fromJson(Map<String, dynamic> parsedJson){
    return Connect(
        account: Account.fromJson(parsedJson['account']),
        token : parsedJson['token']
    );
  }
}

class Account {
  List<dynamic> usernames;
  String id;
  int count;
  String createdAt;
  String updatedAt;
  int v;

  Account({
    this.usernames,
    this.id,
    this.count,
    this.createdAt,
    this.updatedAt,
    this.v
  });

  factory Account.fromJson(Map<String, dynamic> parsedJson){
    return Account(
        usernames: parsedJson['usernames'],
        id: parsedJson['_id'],
        count: parsedJson['count'],
        createdAt: parsedJson['createdAt'],
        updatedAt: parsedJson['updatedAt'],
        v: parsedJson['__v']
    );
  }
}
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'MainApp.dart';
import 'dart:io';

class NewVideo extends StatefulWidget{
  final Connect connect;
  NewVideo({Connect Connect}):connect=Connect;

  @override
  NewVideoState createState() => new NewVideoState();
}

class NewVideoState extends State<NewVideo>{

  List<CameraDescription> _cameras;
  CameraController _controller;
  CameraDescription _activeCamera;
  String _filePath;

  String _users;

  Future<void> _getAvailableCameras() async {
    try {
      _cameras = await availableCameras();
      setState(() {
        _setCameraController(_cameras[0]);
      });
    }
    catch (e) {
      print("_getAvailableCameras $e");
    }
  }

  @override
  void initState() {
    super.initState();
    _getAvailableCameras();
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_controller == null || !_controller.value.isInitialized) {
      return _activeCamera == null ? Placeholder() : Container();
    }

    _users = widget.connect.account.usernames[0].toString();
    if (widget.connect.account.usernames.length > 1)
      for (int i = 1; i >= widget.connect.account.usernames.length; i++)
        _users += widget.connect.account.usernames[i].toString();

    double width = MediaQuery
        .of(context)
        .size
        .width;

    return Scaffold(
      backgroundColor: Colors.black,
      body: new Center(
        child: new AspectRatio(
          aspectRatio: _controller.value.aspectRatio,
          child: new CameraPreview(_controller),
        ),
      ),
      floatingActionButton: _buttons(width),
    );
  }

  Widget _buttons(double width) {
    return new Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        new FloatingActionButton(
          child: Icon(
            Icons.undo,
          ),
          backgroundColor: Colors.transparent,
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        _controller.value.isRecordingVideo ?
        new FloatingActionButton(
          child: Icon(
            Icons.stop,
            color: Colors.red,
          ),
          backgroundColor: Colors.transparent,
          onPressed: () {
            _stopVideoRecording(context);
          },
        ) :
        new FloatingActionButton(
          child: Icon(
            Icons.video_library,
            color: Colors.green,
          ),
          backgroundColor: Colors.transparent,
          onPressed: () {
            _startVideoRecording();
          },
        ),
        new FloatingActionButton(
          child: Icon(
            Icons.autorenew,
          ),
          backgroundColor: Colors.transparent,
          onPressed: () {
            _controller.description == _cameras[0] ? _setCameraController(_cameras[1]) : _setCameraController(_cameras[0]);
          },
        ),
      ],
    );
  }

  String _getTimestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  Future<void> _startVideoRecording() async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/$_users';
    print(dirPath);
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${_getTimestamp()}.mp4';

    if (_controller.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return null;
    }

    try {
      await _controller.startVideoRecording(filePath);
      setState(() {
        _filePath=filePath;
      });
    } on CameraException catch (e) {
      print(e);
    }
  }

  void _setCameraController(CameraDescription cameraDescription) async {
    if (_controller != null) {
      await _controller.dispose();
    }
    _controller = CameraController(cameraDescription, ResolutionPreset.high);
    // If the controller is updated then update the UI.
    _controller.addListener(() {
      if (mounted) setState(() {});
    });

    try {
      await _controller.initialize();
    } on CameraException catch (e) {
      print(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  Future<void> _stopVideoRecording(BuildContext context) async {
    if (!_controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await _controller.stopVideoRecording();
      setState(() {
        Navigator.of(context).pushNamed(
            '/VideoPlayer',
            arguments: _filePath
        );
      });
    } on CameraException catch (e) {
      print(e);
    }
  }
}
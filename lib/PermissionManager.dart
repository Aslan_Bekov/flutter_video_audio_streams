import 'package:simple_permissions/simple_permissions.dart';
import 'dart:io' as IO;
import 'dart:async';

class Permission_Manager {

  Future get_permissions(Permission permission) async
  {
    var _check;
    if (IO.Platform.isAndroid) {
      try{
        _check = await SimplePermissions.checkPermission(permission);
        print('$permission : $_check');
      }
      catch (ex){
        print(ex.toString());
      }
      if (!_check) {
        try{
          var request = await SimplePermissions.requestPermission(permission);
          print(request);
        }
        catch(ex){
          print(ex.toString());
        }
      }
    }
  }
}
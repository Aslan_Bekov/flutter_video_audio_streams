import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() => new HomeState();
}

class HomeState extends State<Home>{
  @override
  Widget build(BuildContext context) {

    double height = MediaQuery
        .of(context)
        .size
        .height;

    return new Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _myButton('Login', height),
            SizedBox(
                height: height * 0.05
            ),
            _myButton('Registration', height)
          ],
        ),
      ),
    );
  }

  Widget _myButton(String button, double height){
    return new MaterialButton(
      onPressed: () {
        Navigator.of(context).pushNamed(
          '/LogOrReg',
          arguments: button
        );
      },
      child: new SizedBox(
        height: height * 0.1,
        child: DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.indigo,
          ),
          child: Center(
            child: Text(
              button,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white
              ),
              textScaleFactor: height * 0.0025,
            ),
          ),
        ),
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:flutterr/Logging/LogOrReg.dart';

class ResponseLogOrReg extends StatefulWidget{
  final Error_https ErrorHttps;
  ResponseLogOrReg({Error_https Error_Https}):ErrorHttps = Error_Https;

  @override
  ResponseLogOrRegState createState() => new ResponseLogOrRegState();
}

class ResponseLogOrRegState extends State<ResponseLogOrReg> {

  @override
  Widget build(BuildContext context) {
      return new AlertDialog(
        title: new Text(widget.ErrorHttps.message),
        actions: [
          new FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('Ok'),
          ),
        ],
      );
    }
}


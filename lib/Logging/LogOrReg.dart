import 'package:flutter/material.dart';
import 'package:flutterr/MainApp.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class LogOrReg extends StatefulWidget{
  final String _state;
  LogOrReg({String state}):_state = state;

  @override
  LogOrRegState createState() => new LogOrRegState();
}

class LogOrRegState extends State<LogOrReg> {

  int _nomber = 1;
  List<Map<String, String>> _users = [
    {'username': '',
      'password': ''}
  ];

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    double height = MediaQuery
        .of(context)
        .size
        .height;

    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.indigo[600],
        title: new Text(widget._state),
        actions: <Widget>[
          new MaterialButton(
            onPressed: () {
              if(_formKey.currentState.validate()){
                _formKey.currentState.save();
                _sendRequest();
              }
            },
            child: new Icon(
              Icons.check_circle_outline,
              color: Colors.white,
            ),
          )
        ],
      ),
      floatingActionButton: _FloatingActionButton(width),
      body: new Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: _passText(width, height)
      ),
    );
  }

  Widget _buttonAdd(){
    return new FloatingActionButton(
      onPressed: () {
        setState(() {
          _nomber++;
          Map<String, String> _user = {
            'username': '',
            'password': ''
          };
          _users.add(_user);
        });
      },
      backgroundColor: Colors.indigo,
      child: Icon(
        Icons.add,
        color: Colors.white,
      ),
    );
  }

  Widget _FloatingActionButton(double width) {
    if (_nomber == 1) {
      return _buttonAdd();
    }
    else {
      return new Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          new FloatingActionButton(
            onPressed: () {
              setState(() {
                _nomber--;
                _users.removeLast();
              });
            },
            backgroundColor: Colors.indigo,
            child: Icon(
              Icons.remove,
              color: Colors.white,
            ),
          ),
          new SizedBox(
            width: width * 0.01,
          ),
          _buttonAdd(),
        ],
      );
    }
  }

  Widget _passText(double width, double height) {
    return new ListView.builder(
        padding: EdgeInsets.symmetric(
            vertical: height * 0.01, horizontal: width * 0.01),
        itemCount: _nomber,
        itemBuilder: (BuildContext _context, int i) {
          return _textTile(height, i);
        }
    );
  }

  Widget _textTile(double height, int index) {
    int _index = index + 1;
    return new ListTile(
        title: Column(
            children: [
              Text(
                'user №' + (_index).toString(),
              ),
              TextFormField(
                validator: validateText,
                onSaved: (String value) {
                  _users[index]['username'] = value;
                },
                onEditingComplete: () {

                },
                decoration: InputDecoration(
                  labelText: 'Enter your username',
                  icon: Icon(Icons.person),
                ),
              ),
              TextFormField(
                validator: validateText,
                obscureText: true,
                onSaved: (String value) {
                  _users[index]['password'] = value;
                },
                decoration: InputDecoration(
                  labelText: 'Enter your password',
                  icon: Icon(Icons.vpn_key),
                ),
              ),
              SizedBox(height: height * 0.02,),
              Divider()
            ]
        )
    );
  }

  String validateText(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Data';
    else
      return null;
  }

  _sendRequest() async {
    String body, url;
    int status;

    try {
      widget._state == 'Login' ?
      url = 'https://something-more.dev.pfrus.com/auth/login' : url = 'https://something-more.dev.pfrus.com/auth/register';


      var _httpsbody = jsonEncode({
        "count": _users.length,
        "credentialPairs": _users
      });

      var response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: _httpsbody
      );

      status = response.statusCode;
      body = response.body;
      print('_body = $body || _status = $status');

      if (status == 401) {
        final jsonResponse = json.decode(response.body);
        Error_https erorhttps = Error_https.fromJson(jsonResponse);

        Navigator.of(context).pushNamed(
            '/ResponseLOR',
            arguments: erorhttps
        );
      }
      else if (status == 200) {
        final jsonResponse = json.decode(response.body);
        Connect connect = Connect.fromJson(jsonResponse);

        Navigator.of(context).pushNamedAndRemoveUntil(
            '/MainApp',
            ((Route<dynamic> route) => false),
            arguments: connect
        );
      }
    }
    catch (error) {
      status = 0;
      body = error.toString();
      print('_body = $body || _status = $status');

      Error_https erorhttps=Error_https(message: body, status: status);
      Navigator.of(context).pushNamed(
          '/ResponseLOR',
          arguments: erorhttps
      );
    }
//    setState(() {});
  }
}

class Error_https {
  String message;
  int status;

  Error_https({
    this.message,
    this.status
  });

  factory Error_https.fromJson(Map<String, dynamic> parsedJson){
    return Error_https(
        message: parsedJson['message'],
        status : parsedJson['status']
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutterr/Logging/Home.dart';
import 'package:flutterr/Logging/LogOrReg.dart';
import 'package:flutterr/Logging/ResponseLogOrReg.dart';
import 'package:flutterr/MainApp.dart';
import 'package:flutterr/NewVideo.dart';
import 'package:flutterr/PermissionManager.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:flutterr/VideoPlayer.dart';
import 'package:flutterr/VideoStream.dart';

void main() async {
  Permission_Manager pm = Permission_Manager();
//  var res1 = await pm.get_permissions(Permission.ReadPhoneState);
  var res2 = await pm.get_permissions(Permission.RecordAudio);
  var res = await pm.get_permissions(Permission.Camera);

  return runApp(
      new MaterialApp(
        title: 'Флатер',
        initialRoute: '/',
        routes: <String, WidgetBuilder>{
          '/': (BuildContext context) => new Home()
        },
        onGenerateRoute: (routeSettings) {
          switch (routeSettings.name) {
            case '/LogOrReg':
              return new MaterialPageRoute(
                builder: (context) =>
                new LogOrReg(state: routeSettings.arguments),
                settings: routeSettings,
              );
              break;
            case '/ResponseLOR':
              return new PageRouteBuilder(
                opaque: false,
                pageBuilder: (context, _, __) =>
                new ResponseLogOrReg(Error_Https: routeSettings.arguments),
                settings: routeSettings,
              );
              break;
            case '/MainApp':
              return new MaterialPageRoute(
                builder: (context) =>
                new MainApp(Connect: routeSettings.arguments),
                settings: routeSettings,
              );
              break;
            case '/NewVideo':
              return new MaterialPageRoute(
                builder: (context) =>
                new NewVideo(Connect: routeSettings.arguments),
                settings: routeSettings,
              );
              break;
            case '/VideoPlayer':
              return new MaterialPageRoute(
                builder: (context) =>
                new VideoPlayer(FilePath: routeSettings.arguments),
                settings: routeSettings,
              );
              break;
            case '/VideoStream':
              return new MaterialPageRoute(
                builder: (context) =>
                new VideoStream(connect: routeSettings.arguments),
                settings: routeSettings,
              );
              break;
          }
        },
      )
  );
}
import 'package:flutter/material.dart';
import 'dart:io';
import 'MainApp.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'package:flutter/services.dart.';

class VideoStream extends StatefulWidget {
  final Connect connect;
  VideoStream({Connect this.connect});

  @override
  VideoStreamState createState() => new VideoStreamState();
}

class VideoStreamState extends State<VideoStream>{

  List<CameraDescription> _cameras;
  CameraController _controller;
  CameraDescription _activeCamera;
  String _filePath;

  bool _isScanBusy=false;

  String _users;

  Future<void> _getAvailableCameras() async {
    try {
      _cameras = await availableCameras();
      setState(() {
        _setCameraController(_cameras[0]);
      });
    }
    catch (e) {
      print("_getAvailableCameras $e");
    }
  }

  void _setCameraController(CameraDescription cameraDescription) async {
    if (_controller != null) {
      await _controller.dispose();
    }

    _controller = CameraController(cameraDescription, ResolutionPreset.high);
    _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }

      _controller.startImageStream((CameraImage availableImage) {
        _controller.stopImageStream();
        _scanText(availableImage);
      });

      setState(() {});
    });
//    _controller.addListener(() {
//      if (mounted) setState(() {});
//    });
//
//    try {
//      await _controller.initialize();
//    } on CameraException catch (e) {
//      print(e);
//    }
//
//    if (mounted) {
//      setState(() {});
//    }
  }

  void _scanText(CameraImage availableImage) async {

    _isScanBusy = true;

    print("scanning!...");

    // final FirebaseVisionImageMetadata metadata =
    //     FirebaseVisionImageMetadata(
    //   rawFormat: 35,
    //   size: const Size(1.0, 1.0),
    //   planeData: <FirebaseVisionImagePlaneMetadata>[
    //     FirebaseVisionImagePlaneMetadata(
    //       bytesPerRow: 1000,
    //       height: 480,
    //       width: 480,
    //     ),
    //   ],
    // );

//    final FirebaseVisionImageMetadata metadata = FirebaseVisionImageMetadata(
//        rawFormat: availableImage.format.raw,
//        size: Size(1.0, 1.0),
//        planeData: availableImage.planes.map((currentPlane) => FirebaseVisionImagePlaneMetadata(
//            bytesPerRow: currentPlane.bytesPerRow,
//            height: currentPlane.height,
//            width: currentPlane.width
//        )).toList()
//    );
//
//    final FirebaseVisionImage visionImage = FirebaseVisionImage.fromBytes(availableImage.planes[0].bytes, metadata);
//    final TextRecognizer textRecognizer = FirebaseVision.instance.textRecognizer();
//    final VisionText visionText = await textRecognizer.processImage(visionImage);
//
//    print(visionText.text);
//    for (TextBlock block in visionText.blocks) {
//      // final Rectangle<int> boundingBox = block.boundingBox;
//      // final List<Point<int>> cornerPoints = block.cornerPoints;
//      print(block.text);
//      final List<RecognizedLanguage> languages = block.recognizedLanguages;
//
//      for (TextLine line in block.lines) {
//        // Same getters as TextBlock
//        print(line.text);
//        for (TextElement element in line.elements) {
//          // Same getters as TextBlock
//          print(element.text);
//        }
//      }
//    }

    _isScanBusy = false;
  }

  @override
  void initState() {
    super.initState();
    _getAvailableCameras();
  }

  @override
  Widget build(BuildContext context) {
    return null;
  }
}